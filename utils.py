from IPython.core.display import clear_output
import sys, os, string, glob
from numpy import *
import time

ln = log
log = log10
concat = concatenate
roman = [None,'I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII','XIII','XIV','XV','XVI','XVII','XVIII','XIV','XX','XXI','XXII','XXIII','XXIV','XXV','XXVI','XXVII','XXVIII','XXIX','XXX']

noFilter = lambda k: True
dummyFunc = lambda x: x

def lens(arr):
    return array([len(x) for x in arr])

def iround(f,modulo=1):
    if isinstance(f,ndarray):
        if modulo >= 1: return round(f/modulo).astype(int)*modulo
        else: return round(f/modulo).astype(int) / (1/modulo)  #patch for floating point bug
    if modulo >= 1: return int(round(f/modulo))*modulo
    else: return int(round(f/modulo)) / (1/modulo)  #patch for floating point bug

def filelines(fn):
    f = file(fn)
    ls = f.readlines()
    f.close()
    return ls


def rl(arr,lim=0):
    if lim==0:
        return range(len(arr))
    if lim<0:
        return range(len(arr)+lim)
    if lim>0:
        return range(lim,len(arr))
def searchsortedclosest(arr, val):
    assert(arr[0]!=arr[1])
    if arr[0]<arr[1]:
        ind = searchsorted(arr,val)
        ind = minarray(ind, len(arr)-1)
        return maxarray(ind - (val - arr[maxarray(ind-1,0)] < arr[ind] - val),0)        
    else:
        ind = searchsorted(-arr,-val)
        ind = minarray(ind, len(arr)-1)
        return maxarray(ind - (-val + arr[maxarray(ind-1,0)] < -arr[ind] + val),0)        



def unzip(tupslist):
    l = min(lens(tupslist))
    return [[tups[i] for tups in tupslist] for i in range(l)]


def searchAndInterpolate(arr_xs,val,arr_ys): # beyond edges takes edge values
    ind = searchsorted(arr_xs,val)
    if ind==0: return arr_ys[0]
    if ind==len(arr_xs): return arr_ys[-1]
    if arr_xs[ind]==val: return arr_ys[ind]
    return lineFunc(arr_xs[ind-1], arr_ys[ind-1], arr_xs[ind], arr_ys[ind])(val)
def floatCanFail(s):
    try:
        return float(s)
    except:
        return s
def sumlst(lst):
    return reduce(lambda x,y: x+y,lst)
def mifkad(arr):
    dic={}
    for x in arr:
        if dic.has_key(x):
            dic[x] += 1
        else:
            dic[x] = 1
    return dic
def cartesianMultiplication(lsts, init=None):
    if len(lsts)==1 and init==None: return [[l] for l in lsts[0]]
    if len(lsts)==0: return init
    prevlsts = cartesianMultiplication(lsts[:-1],init=init)
    newlsts = sumlst([[lst + [x] for x in lsts[-1]] for lst in prevlsts])
    return newlsts
class Progress:
    def __init__(self, iterable,name='progress',frac=None,fmt='%d',dummy=False):
        self.iterable = iterable		
        self.fmt=fmt
        self.dummy=dummy
        if frac!=None: self.frac = frac
        else: 
            if len(self.iterable)>1: self.frac = max(0.01, 1./(len(self.iterable)-1.))
            else: self.frac = 1.
        self.name=name
    def __iter__(self):
        self.origIter = self.iterable.__iter__()
        self.startTime = time.time()
        self.i = 0.
        self.looplen = len(self.iterable)
        self.printEvery = iround(self.looplen*self.frac)
        return self
    def next(self):
        if not self.dummy and self.printEvery>0:
            if self.i%self.printEvery==0.: 
                if self.looplen>0:
                    clear_output()					
                    print '%s: '%self.name, self.fmt%(self.i*100./self.looplen) + '%', int(time.time()-self.startTime), 'seconds passed',
                sys.stdout.flush()
        self.i+=1
        try:
            return self.origIter.next()
        except StopIteration:
            if not self.dummy and self.printEvery>0: 
                print
                sys.stdout.flush()
            raise StopIteration




def processinfo(title):
    print title
    print 'module name:', __name__
    print 'parent process:', os.getppid()
    print 'process id:', os.getpid()	
def out(arr,fmt=None):
    for a in arr:
        if fmt==None:
            print a
        else:
            print fmt%a


class myDict(dict):
    def valuesSortedByKey(self):
        return [x[1] for x in sorted(self.items())]


def lineFunc(x1,y1,x2,y2,pr=False):
    a = (y2-y1) * 1. / (x2-x1)
    b = y1
    if pr: print "y=%.2f x + %.2f"%(a, b-a*x1)
    return lambda x: a*(x-x1)+b

def maxarray(arr, v):
    return arr + (arr<v)*(v-arr)
def minarray(arr, v):
    return arr + (arr>v)*(v-arr)
def ismonotonic(arr,maxDiffForEqual=1e-10):
    return ( (-1 not in sign(arr[1:]-arr[:-1]+maxDiffForEqual)) or
             (1 not in sign(arr[1:]-arr[:-1]-maxDiffForEqual)) )
def searchAndInterpolate1D(arr_xs,valx,arr_ys): # beyond edges extrapolates from last two points
    i = min( max(searchsorted(arr_xs, valx), 1), len(arr_xs)-1)

    dx = arr_xs[i] - arr_xs[i-1]
    xn = 1.* (valx - arr_xs[i-1]) / dx

    return arr_ys[i-1]*(1-xn) + arr_ys[i]*xn

IonizationEnergies = [((1, 'H', 'hydrogen'), [1312.0]),
                      ((2, 'He', 'helium'), [2372.3, 5250.5]),
                      ((3, 'Li', 'lithium'), [520.2, 7298.1, 11815.0]),
                      ((4, 'Be', 'beryllium'), [899.5, 1757.1, 14848.7, 21006.6]),
                      ((5, 'B', 'boron'), [800.6, 2427.1, 3659.7, 25025.8, 32826.7]),
                      ((6, 'C', 'carbon'), [1086.5, 2352.6, 4620.5, 6222.7, 37831.0, 47277.0]),
                      ((7, 'N', 'nitrogen'),[1402.3, 2856.0, 4578.1, 7475.0, 9444.9, 53266.6, 64360.0]),
                      ((8, 'O', 'oxygen'),  [1313.9, 3388.3, 5300.5, 7469.2, 10989.5, 13326.5, 71330.0, 84078.0]),
                      ((9, 'F', 'fluorine'),  [1681.0,   3374.2,   6050.4,   8407.7,   11022.7,   15164.1,   17868.0,   92038.1,   106434.3]),
                      ((10, 'Ne', 'neon'),  [2080.7,   3952.3,   6122.0,   9371.0,   12177.0,   15238.0,   19999.0,   23069.5,   115379.5,   131432.0]),
                      ((11, 'Na', 'sodium'),  [495.8,   4562.0,   6910.3,   9543.0,   13354.0,   16613.0,   20117.0,   25496.0,   28932.0,   141362.0,   159076.0]),
                      ((12, 'Mg', 'magnesium'),  [737.7,   1450.7,   7732.7,   10542.5,   13630.0,   18020.0,   21711.0,   25661.0,   31653.0,   35458.0,   169988.0,   189368.0]),
                      ((13, 'Al', 'aluminium'),  [577.5,   1816.7,   2744.8,   11577.0,   14842.0,   18379.0,   23326.0,   27465.0,   31853.0,   38473.0,   42647.0]),
                      ((14, 'Si', 'silicon'),  [786.5,   1577.1,   3231.6,   4355.5,   16091.0,   19805.0,   23780.0,   29287.0,   33878.0,   38726.0,   45962.0,   50502.0,   235196.0,   257923.0]),
                      ((15, 'P', 'phosphorus'),  [1011.8,   1907.0,   2914.1,   4963.6,   6273.9,   21267.0,   25431.0,   29872.0,   35905.0,   40950.0,   46261.0,   54110.0,   59024.0,   271791.0,   296195.0]),
                      ((16, 'S', 'sulfur'),  [999.6,   2252.0,  3357.0,   4556.0,   7004.3,   8495.8,   27107.0,   31719.0,   36621.0,   43177.0,   48710.0,   54460.0,   62930.0,   68216.0,   311048.0,   337138.0]), 
                      ((17, 'Cl', 'chlorine'),  [1251.2,   2298.0,   3822.0,   5158.6,   6542.0,   9362.0,   11018.0,   33604.0,   38600.0,   43961.0,   51068.0,   57119.0,   63363.0,   72341.0,   78095.0,   352994.0,   380760.0]),
                      ((18, 'Ar', 'argon'),  [1520.6,   2665.8,   3931.0,   5771.0,   7238.0,   8781.0,   11995.0,   13842.0,   40760.0,   46186.0,   52002.0,   59653.0,   66199.0,   72918.0,   82473.0,   88576.0,   397605.0,   427066.0]),
                      ((19, 'K', 'potassium'),  [418.8,   3052.0,   4420.0,   5877.0,   7975.0,   9590.0,   11343.0,   14944.0,   16963.7,   48610.0,   54490.0,   60730.0,   68950.0,   75900.0,   83080.0,   93400.0,   99710.0,   444880.0,   476063.0]),
                      ((20, 'Ca', 'calcium'),  [589.8,   1145.4,   4912.4,   6491.0,   8153.0,   10496.0,   12270.0,   14206.0,   18191.0,   20385.0,   57110.0,   63410.0,   70110.0,   78890.0,   86310.0,   94000.0,  104900.0,   111711.0,   494850.0,   527762.0]),
                      ((21, 'Sc', 'scandium'),  [633.1,   1235.0,   2388.6,   7090.6,   8843.0,   10679.0,  13310.0,   15250.0,   17370.0,   21726.0,   24102.0,   66320.0,   73010.0,   80160.0,   89490.0,   97400.0,   105600.0,   117000.0,   124270.0,   547530.0,   582163.0]),
                      ((22, 'Ti', 'titanium'),  [658.8,   1309.8,   2652.5,   4174.6,   9581.0,   11533.0,   13590.0,   16440.0,   18530.0,   20833.0,   25575.0,   28125.0,   76015.0,   83280.0,   90880.0,   100700.0,   109100.0,   117800.0,   129900.0,   137530.0,   602930.0,   639294.0]),
                      ((23, 'V', 'vanadium'),  [650.9,   1414.0,   2830.0,   4507.0,   6298.7,   12363.0,   14530.0,   16730.0,   19860.0,   22240.0,   24670.0,   29730.0,   32446.0,   86450.0,   94170.0,   102300.0,   112700.0,   121600.0,   130700.0,   143400.0,   151440.0,   661050.0,   699144.0]),
                      ((24, 'Cr', 'chromium'),  [652.9,   1590.6,   2987.0,   4743.0,   6702.0,   8744.9,   15455.0,   17820.0,   20190.0,   23580.0,   26130.0,   28750.0,   34230.0,   37066.0,   97510.0,   105800.0,   114300.0,   125300.0,   134700.0,   144300.0,   157700.0,   166090.0,   721870.0,   761733.0]),
                      ((25, 'Mn', 'manganese'),  [717.3,   1509.0,   3248.0,   4940.0,   6990.0,   9220.0,   11500.0,   18770.0,  21400.0,   23960.0,   27590.0,   30330.0,   33150.0,   38880.0,   41987.0,   109480.0,   118100.0,   127100.0,   138600.0,   148500.0,   158600.0,   172500.0,   181380.0,   785450.0,   827067.0]),
                      ((26, 'Fe', 'iron'),  [762.5,   1561.9,   2957.0,   5290.0,  7240.0,   9560.0,   12060.0,   14580.0,   22540.0,   25290.0,   28000.0,   31920.0,   34830.0,  37840.0,   44100.0,   47206.0,   122200.0,   131000.0,  140500.0,   152600.0,   163000.0,   173600.0,   188100.0,   195200.0,   851800.0,   895161.0]),
                      ((28, 'Ni', 'nickel'), [737.1,1753.0,3395.,5300.,7339.,10400.,12800.,15600.,18600.,21670,30970.,34000.,37100.,41500.,44800.,48100.,55101.,58570.,148700.,159000.,169400.,182700.,194000.,205600.,221400.,231490.,992718.,1039668.]),
                      ((30, 'Zn', 'zinc'), [906.4])
                      ]

IonizationEnergiesDic = dict([(x[0][1], (x[0][0],x[0][2],x[1])) for x in IonizationEnergies])


rydberg2Angstrom = lambda ryd: 1000/1.0968 / ryd

def choiceWithReplacement(lst, nChoices=None,use_np=True):  #this func exists in new versions of numpy.random
    if nChoices==None: nChoices=len(lst)
    inds = numpy.random.randint(0,len(lst),nChoices)
    if use_np:
        return array(lst).take(inds).tolist()
    else:
        return [lst[i] for i in inds]
def group(func, arr):
    dic = {}
    for a in arr:
        if dic.has_key(func(a)):
            dic[func(a)].append(a)
        else:
            dic[func(a)] = [a]
    return dic

