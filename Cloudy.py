import time, subprocess, multiprocessing, os, shutil, string, glob
import utils as u
from pylab import *
from utils import log, array, rl, arange, concat, searchsorted, searchsortedclosest, unzip, out

class Element:
    solarAbundances = 10**array([0.,-1.0000, -3.6108,-4.0701,-3.3098,-4.0000,-5.6696,-4.4597,-5.5302,-4.4597,-4.7352,-6.7190,-5.6003,-5.6402,-4.5498 ,-5.7496])
    MetalNames =           'h','he',    'c',    'n',    'o',    'ne',   'na',   'mg',   'al',   'si',   's',    'cl',   'ar',   'ca',   'fe',    'ni'	
    solarAbundances = dict([(MetalNames[i],solarAbundances[i]) for i in rl(MetalNames)]) 
    Asplund09Abundances = {'O':8.69,'Si':7.51,'N':7.83}
    depletions = {'Na':0.15,'Ca':10**-3.75}
    def __init__(self, name):
	self.name = name
    def shortname(self):
	elementLetter = self.name[0].upper() + ('',self.name[1])[self.name in ('argon','neon','silicon','aluminium','helium','nickel')]
	if self.name=='iron': elementLetter='Fe'
	if self.name=='magnesium': elementLetter='Mg'
	if self.name=='sodium': elementLetter='Na'
	if self.name=='zinc': elementLetter='Zn'
	if self.name=='calcium': elementLetter='Ca'
	return elementLetter
    def nElectrons(self):
	return u.IonizationEnergiesDic[self.shortname()][0]
    def __str__(self):
	return self.shortname()
    def __repr__(self):
	return self.__str__()
    def abundance(self,abundanceSet='solar'):
	if abundanceSet=='solar': return self.solarAbundances[self.shortname().lower()]
	if abundanceSet=='depleted': return self.solarAbundances[self.shortname().lower()] * self.depletions.get(self.shortname(),1.)
	
    
elementList = [Element(name) for name in ('hydrogen','helium','sulphur', 'nitrogen', 'neon','argon','sodium','iron','oxygen','magnesium','carbon','silicon','aluminium','nickel','zinc','calcium')]
elementListDic = dict([(el.shortname(),el) for el in elementList])
class Ion:
    def __init__(self, s,ionLevel=None):
	if isinstance(s,Ion):
	    self.el = s.el
	    self.ionLevel = s.ionLevel
	else:
	    if s[-1]=='*':
		self.excited = True
		s = s[:-1]
	    else:
		self.excited = False
	    for el in sorted(elementList,key=lambda el: -len(str(el))):
		if str(el)==s[:len(str(el))]:
		    self.el = el
		    if ionLevel!=None: self.ionLevel = ionLevel
		    else: self.ionLevel = u.roman.index(s[len(str(el)):])-1
		    break
    def ionizationEnergy(self,eV=False):
	if eV: fact = 1312./13.6
	else: fact = 1312. #rydberg
	if self.ionLevel==0: return 0.
	return u.IonizationEnergiesDic[str(self.el)][2][self.ionLevel - 1] / fact
    def __hash__(self):
	return hash(self.__str__())
    def __str__(self):
	s = '%s%s'%(self.el, u.roman[self.ionLevel+1])
	if not self.excited:
	    return s
	else:
	    return '%s*'%s
    def __repr__(self):
	return self.__str__()
    def __eq__(self,other):
	if type(other)==type(''):
	    return self.__str__()==other
	if isinstance(other, Ion):
	    return self.__str__()==other.__str__()
	return False
    def __ne__(self,other):
	return not self.__eq__(other)
	

class CloudyLineOutput:
    def __init__(self,energy,cloudyname,intrinsicI,emergentI,linetype):
	self.wl = u.rydberg2Angstrom(energy)
	self.cloudyname = cloudyname
	self.intrinsicI = intrinsicI
	self.emergentI = emergentI
	self.linetype = linetype
    def __str__(self):
	return "%d: %s %.1f"%(self.wl,self.cloudyname,log(self.emergentI))
    def __repr__(self):
	return self.__str__()
class EmissionLine:
    def __init__(self,cloudyName, element=None, ionizationLevel=None, label=None, ncrit=None,
                 transitionProb=None,colStrength=None,statWeights=None,isMolecule=False,isCO=False,iCO=None,
                 isForbidden=None,isRecombination=None):
	self.cloudyName = cloudyName	
	ss = string.split(cloudyName)
	
	if ss[-1][-1]=='m': self.wl = float(ss[-1][:-1])*1e4
	else: self.wl = float(ss[-1])

	self._label = label
	self._isForbidden = isForbidden
	self._isRecombination = isRecombination
	self.transitionProb = transitionProb
	self.colStrength = colStrength
	self.isMolecule=isMolecule
	self.isCO=isCO
	if self.isCO: 
	    self.iCO=iCO
	    self.isMolecule = True
	if statWeights!=None: self.statWeights = {'top':statWeights[0]*1., 'bottom':statWeights[1]*1.}
	if element==None: 
	    self.element = ss[0][0].upper()
	    if len(ss[0])>=2: self.element+=ss[0][1]
	else: self.element = element
	if ionizationLevel==None:
	    if len(ss)>2: sionlevel = ss[1]
	    elif len(ss[0])>2:
		if ss[0][2]=='F': sionlevel = '2' #for CaII 7291
		else: sionlevel = ss[0][2:4]		
	    else: 
		sionlevel=None		
	    if sionlevel==None: self.ionizationLevel=sionlevel
	    elif sionlevel[-1].isdigit(): self.ionizationLevel = int(sionlevel)
	    elif sionlevel[-1] in u.roman: self.ionizationLevel = u.roman.index(sionlevel)	    
	else: self.ionizationLevel = ionizationLevel
	self.ncrit = ncrit
    def upperCaseName(self):
	s = self.cloudyName.upper()
	if not self.isMolecule and (s[2]==' ' or s[:4]=='CAF1'): s = s[0] + s[1].lower() + s[2:]
	if s[-1]!='M': s+='A'
	else: s=s[:-1]+'m'
	return string.join(string.split(s),' ')
    def isForbidden(self):
	if self._isForbidden!=None: return self._isForbidden
	return self.element not in ('H','He')
    def isRecombination(self):
	if self._isRecombination!=None: return self._isRecombination
	return self.element in ('H','He')    
    def label(self,onlyelement=False,onlywl=False,python=False):
	if self._label!=None: return self._label
	if not self.isMolecule: elementLabel = '%s%s %s%s'%( ('','[')[self.isForbidden()], self.element, u.roman[self.ionizationLevel], ('',']')[self.isForbidden()])
	elif self.isCO: elementLabel = 'CO J(%d-%d)'%(self.iCO+1,self.iCO)
	wlLabel = '%s'%string.split(self.cloudyName)[-1]
	if wlLabel[-1]=='m': wlLabel = wlLabel[:-1] + (r'$\mu$m','m')[python]
	else: wlLabel = wlLabel + (r'\AA','A')[python]
	if onlyelement: return elementLabel
	if onlywl: return wlLabel
	return elementLabel + ' ' + wlLabel
    def key(self):
	return self.wl
    def ionizationPotential(self,eV=True):
	assert(self.ionizationLevel!=False)
	if eV: fact = 1312./13.6
	else: fact = 1312.
	if not self.isRecombination() and self.ionizationLevel==1: return 0.
	return IonizationEnergiesDic[self.element][2][self.ionizationLevel + self.isRecombination() - 2] / fact
    def hnu(self):
	h = 6.62606957e-27	
	return h * 3e18 / self.wl
    def qUp(self,T=1e4):
	return self.statWeights['top']/self.statWeights['bottom']*self.qDown(T)*exp(-self.hnu()/(kB*T))
    def qDown(self,T=1e4):
	return 8.629e-6 * self.colStrength * T**-0.5 / self.statWeights['top']
    def coolingRate(self,ne,T=1e4):
	return self.qUp()*self.hnu() / (1.+ne*self.qDown()/self.transitionProb)
    def __str__(self):
	return self.label(python=True)
    def __repr__(self):
	return self.label(python=True)
class SED:
    waveunits = 'Hz','rydberg','ev','kev','A','mic'
    intensityunits = 'Jnu','Fnu','nuFnu','Lnu','nuLnu','Llambda','Flambda'      #always in [ergs s^-1] and if applicable: [cm^-2], [sr^-1], [Hz^-1]
    def copyConstructor(self,other):
	assert(isinstance(other, SED))
	self.dlognu = other.dlognu
	self._nus = array(other._nus)
	self._intensities = array(other._intensities)
	self.unit = other.unit	
	self.cloudynuRange = other.cloudynuRange
	self.key = other.key
    def __init__(self, constructorInput=None, waveunit='Hz', intensityunit='Fnu', isLog=(False,False),
                 resolution=0.01,cloudynuRange=(None,None),k=None,other=None): 
	rydberg = 1/1.0968e5 #cm
	Rydbergnu = 3e10/rydberg
	eV = 1.602176565e-12
	h = 6.62606957e-27
	
	assert(waveunit in self.waveunits)
	assert(intensityunit in self.intensityunits)
	if other!=None: self.copyConstructor(other)
	else:
	    if isinstance(constructorInput,tuple):
		tuples = u.unzip(constructorInput)
	    elif isinstance(constructorInput,list):
		if isinstance(constructorInput[0],str):
		    tuples = [map(float, string.split(l)) for l in constructorInput]
		else:
		    tuples = constructorInput
	    if isLog[0]:
		for itup, tup in enumerate(tuples): tuples[itup] = [10**tup[0],tup[1]]
	    if isLog[1]:
		for itup, tup in enumerate(tuples): tuples[itup] = [tup[0],10**tup[1]]
	    if waveunit in ('A','mic'): tuples = tuples[::-1]
	    
	    if waveunit=='Hz':      nus = [x[0] for x in tuples]
	    if waveunit=='rydberg': nus = [x[0] * Rydbergnu for x in tuples]
	    if waveunit=='ev':      nus = [x[0] * eV / h for x in tuples]
	    if waveunit=='kev':     nus = [x[0] * 1000. * eV / h for x in tuples]
	    if waveunit=='A':       nus = [3e18 / x[0] for x in tuples]
	    if waveunit=='mic':     nus = [3e14 / x[0] for x in tuples]
	    
	    self.dlognu = resolution
	    minlnu = u.iround(log(nus[0]),self.dlognu)
	    if minlnu < log(nus[0]): minlnu += self.dlognu
	    maxlnu = u.iround(log(nus[-1]),self.dlognu)
	    if maxlnu < log(nus[-1]): maxlnu += self.dlognu
	    self._nus = 10**arange(minlnu,maxlnu,self.dlognu)
	    if intensityunit[1:]=='lambda': 
		assert(waveunit=='A')
		self.unit = intensityunit[0]
		intensities = array([x[0]*x[1] for x in tuples])
	    else: 
		self.unit = intensityunit[-3]
		intensities = array([x[1] for x in tuples])
	    self._intensities = 10.**array([u.searchAndInterpolate(log(nus), log(nu), log(intensities)) for nu in self._nus])
	    if intensityunit[:2]=='nu' or intensityunit[1:]=='lambda': self._intensities /= self._nus
	    self.cloudynuRange = cloudynuRange
	    self.key = k
    def nus(self): return self._nus  
    def minnu(self): return self.nus()[0]
    def maxnu(self): return self.nus()[-1]
    def wls(self): return 3e18/self.nus()
    def rydbergs(self): 
	rydberg = 1/1.0968e5 #cm
	Rydbergnu = 3e10/rydberg	
	return self.nus() / Rydbergnu 
    def energies(self): 
	rydberg = 1/1.0968e5 #cm
	Rydbergnu = 3e10/rydberg
	eV = 1.602176565e-12
	h = 6.62606957e-27
	RydbergEV = h*Rydbergnu/eV	
	return self.rydbergs() * RydbergEV
    def dnus(self):
        mids = (self.nus()[2:] - self.nus()[:-2])/2.
        return concat([[mids[0]],mids,[mids[-1]]])
    
    def Jnus(self):
	assert(self.unit=='J')
	return self._intensities
    def nuJnus(self):
	return self.Jnus()*self.nus()
    def Fnus(self,r=None):
	if self.unit=='F': return self._intensities
	if self.unit=='J': return self._intensities * 4.*pi / 3. #correct for radiation pressure calculation only
	if self.unit=='L': return self._intensities / (4.*pi*r**2.)
    def toFnus(self,r=None):
	newSED = self.__class__(other=self)
	newSED._intensities = array(newSED.Fnus(r))
	newSED.unit='F'
	return newSED      
    def nuFnus(self,r=None):
	return self.nus()*self.Fnus(r)
    def Lnus(self):
	assert(self.unit=='L')
	return self._intensities
    def nuLnus(self):
	return self.nus() * self.Lnus()
    def Qnus(self,r=None):
	h = 6.62606957e-27	
	return self.Fnus(r) / (h * self.nus())
    def nuQnus(self,r=None):
	return self.Qnus(r)*self.nus()
    def integrate(self,intensities,nurng=None):
	if nurng!=None: s,e = self.nuInd(nurng)
	else: s,e=None,None
	return (intensities*self.dnus())[s:e].sum()
    def __add__(self,other):
	assert(isinstance(other,SED))
	assert(self.unit==other.unit)
	assert(self.dlognu==other.dlognu)
	newSED = self.__class__(other=self)
	minlnu = log(min(self.minnu(), other.minnu()))
	maxlnu = log(max(self.maxnu(), other.maxnu()))
	newSED._nus = 10.**arange(minlnu,maxlnu+newSED.dlognu,newSED.dlognu)
	newSED._intensities = zeros(len(newSED._nus))
	for sed in (self,other):
	    ind = searchsortedclosest(newSED._nus, sed.minnu())
	    l = len(sed.nus())
	    newSED._intensities[ind:ind+l] += sed._intensities
	return newSED
    def addrange(self,other,minnu,maxnu):
	newSED = other.__class__(other=other)
	s,e=newSED.nuInd(array([minnu,maxnu]))
	for i in rl(newSED._nus):
	    if (i<s) or (i>=e): 
		newSED._intensities[i] = 0.
	return self + newSED	
    def __mul__(self,other):
	assert(isinstance(other,float) or isinstance(other,int))
	newSED = self.__class__(other=self)
	newSED._intensities *= other
	return newSED
    def __div__(self,other):
	return self.__mul__(1./other)
    
    def nuInd(self,nu):
	return searchsorted(self.nus(),nu)
    def ionizationInds(self,maxRydberg=1000.):
	rydberg = 1/1.0968e5 #cm
	Rydbergnu = 3e10/rydberg	
	return self.nuInd(Rydbergnu), self.nuInd(Rydbergnu*maxRydberg) 
    def adjustSpec(self,nurange,factor):
	s,e = self.nuInd(nurange)
	self._intensities[s:e]*=factor
    def phi(self,isOpticallyThin,byF=False,maxRydberg=1000.):
	rydberg = 1/1.0968e5 #cm
	Rydbergnu = 3e10/rydberg
	h = 6.62606957e-27
	
	if not byF:
	    if isOpticallyThin:	    
		return self.integrate(self.Jnus() / (h * self.nus()) * 4 * pi, (Rydbergnu, Rydbergnu*maxRydberg))
	    else:
		return self.integrate(self.Jnus() / (h * self.nus()) * 2 * pi, (Rydbergnu, Rydbergnu*maxRydberg))
	else:
	    return self.integrate(self.Fnus() / (h * self.nus()), (Rydbergnu, Rydbergnu*maxRydberg))
    def ng(self,isOpticallyThin,byF=False):
	c = 299792.458 * 1e5  	
	return self.phi(isOpticallyThin,byF) / c
    def meanhnu(self,maxRydberg=1000.):
	rydberg = 1/1.0968e5 #cm
	Rydbergnu = 3e10/rydberg
	
	arbitraryr=1 #in case self.unit=='L'
	num     = self.integrate(self.Fnus(arbitraryr), (Rydbergnu, Rydbergnu*maxRydberg))
	denom   = self.integrate(self.Qnus(arbitraryr), (Rydbergnu, Rydbergnu*maxRydberg))
	return num/denom
    def __str__(self): 
	return str(self.key)
    def __repr__(self):
	return self.__str__()
    def addSlopes(self,slopesBegin,slopesEnd):
	nus = self._nus
	Fnus = self.Fnus()
	for minwl,slope in slopesBegin[::-1]:
	    nus = u.concat([array([3e18/minwl]),nus])
	    newFnu = Fnus[0] / 10**(log(nus[1]/nus[0]) * slope)
	    Fnus = u.concat([array([newFnu]),Fnus])
	for maxwl,slope in slopesEnd:
	    nus = u.concat([nus,array([3e18/maxwl])])
	    newFnu = Fnus[-1] * 10**(log(nus[-1]/nus[-2]) * slope)
	    Fnus = u.concat([Fnus,array([newFnu])])
	return SED((nus,Fnus), waveunit='Hz', intensityunit='Fnu',k=self.key,resolution=self.dlognu)
	
	    
	     
	
    def forCloudy(self,r=1.):
	if self.cloudynuRange[0]!=None: s = searchsorted(self.nus(),self.cloudynuRange[0])
	else: s = 0
	if self.cloudynuRange[1]!=None: e = searchsorted(self.nus(),self.cloudynuRange[1])
	else: e = len(self.nus())
	
	xs = log(self.nus())[s:e]
	ys = log(self.Fnus(r))[s:e]

	for i in rl(ys): 
	    if not isinf(ys[i]): break
	s=i
	for i in range(len(ys)-1,-1,-1): 
	    if not isinf(ys[i]): break
	e=i+1
	xs = xs[s:e]
	ys = ys[s:e]	

	s = ''
	for i in range(0,len(xs),100):
	    prenom = ('continue   ','interpolate')[i==0]
	    s += '%s %s\n'%(prenom, string.join(['(%.4f %.4f)'%(xs[i],ys[i]) for i in range(i,min(i+100,len(xs)))]))
	return s[:-1]


class Model:
    basedir = None
    cloudyexe = None
    outputprefix = '.coutput'
    inputprefix = '.cinput' 
    errorprefix = '.c_errfile'
    cloudyMetalNames = 'he','li','be','b','c','n','o','f','ne','na','mg','al','si','p','s','cl','ar','k','ca','sc','ti','v','cr','mn','fe','co','ni','cu','zn'
   
    ## initialization and writing Cloudy input files
    def __init__(self, modelname, n=None, U=None, Z = 2, aEUV=None,initialPressure = True,
                 ISRF=None,dust=True,columndensity=23,neutralColumn=False,givenSED=None,BB=None, 
                 setPresIoniz=None,turbulence=None,stopT=None,stop_pfrac=None,
                 loadAll=False,modeldir=None,emittedRadiationPressure=True,iterate=True,d2m=None,
                 emissionLines=[],HMz=None,zCMB=None,
                 starburst99fn=None,starburst99logage=None,elementlist=elementList,
                 ionizationSpectrumLoadEnergies=(1.,1000),isGlobule=False,max_nDecs=7,
                 radius=None,luminosity=None,savelevel=2,maxIterations=10,
                 logBreakFreq=None,dlogBreakFlux=None,saveTmap=False,electronTemp=None,
                 densityPowerLawIndex=None,densityPowerLawScale=None,densityPowerLawGivenAsAMD=True,
                 drmax=None,CRB=False,dustyNotDepleted=False,nmaps=17,AV=None,dustType=None):
        self.name = modelname
        self.n = n
        self.U = U 
	self.Z = Z
	self.inputT = electronTemp
        self.hasdust = dust
        self._modeldir = modeldir
	self.colDensity = columndensity
	self.AV = AV
	self.neutralColumn = neutralColumn
	self.ionizationSpectrumLoadEnergies = ionizationSpectrumLoadEnergies
	self.givenSED = givenSED  #SED from SED class
	self.BB = BB
	if d2m!=None: self.d2m = 10.**d2m
	else: self.d2m = None
	self.emittedRadiationPressure = emittedRadiationPressure
	self.loaded = False
	self.CloudyStat = "not loaded / files not written"
	self.setPresIoniz = setPresIoniz
	self.iterate = iterate
	self.turbulence = turbulence	
	self.ISRF = ISRF
	self.HMz = HMz
	self.emissionLines = emissionLines
	self.elements = elementlist
	self.starburst99fn,self.starburst99logage = starburst99fn,starburst99logage
	self.radius = radius
	self.luminosity = luminosity #this is ionizing luminosity
	self.stopT = stopT
	self.CRB = CRB
	self.stop_pfrac = stop_pfrac
	self.maxIterations = maxIterations
	self.savelevel = savelevel
	if not os.path.exists(self.modeldir()[:-1]): os.mkdir(self.modeldir()[:-1])
	self.initialPressure = initialPressure
	self.logBreakFreq = logBreakFreq
	self.dlogBreakFlux = dlogBreakFlux  	
	self.saveTmap = saveTmap
        if loadAll: self.loadAll()
	self.isGlobule = isGlobule
	if (not densityPowerLawGivenAsAMD) or (densityPowerLawIndex==None):
	    self.densityPowerLawIndex = densityPowerLawIndex
	    self.densityPowerLawScale = densityPowerLawScale
	else:
	    self.AMDindex = densityPowerLawIndex
	    self.AMDscale = densityPowerLawScale
	    if not self.isGlobule:
		self.densityPowerLawIndex  = 1./densityPowerLawIndex
		self.densityPowerLawScale = self.AMDscale - log( self.AMDindex * u.ln(10.) )
	    else:
		self.max_nDecs = max_nDecs
		self.densityPowerLawIndex = 1. / (1-self.AMDindex)
		self.densityPowerLawScale = self.AMDscale - (self.n + log(u.ln(10)*(1-self.AMDindex)))
	self.drmax = drmax
	self.dustyNotDepleted=dustyNotDepleted
	self.zCMB = zCMB
	self.nmaps = nmaps
	self.dustType = dustType
    def __call__(self, fname, *args,**kwargs):	
	if type(fname)==type(''):
	    f=lambda x: x
	    if fname[-2:]=='_l': 
		f = lambda x: u.log(x)
		fname=fname[:-2]
	    if fname[-2:]=='_n':
		f = lambda x: not x
		fname=fname[:-2]
	    if not hasattr(self, fname): raise AttributeError("cloudy.Model has no attribute '%s'"%fname)
	    member = getattr(self,fname)
	    if type(member)==type(self.loadAll): 	    
		return f(getattr(self,fname)(*args,**kwargs))
	    else: 
		return f(getattr(self,fname))
		
	else:
	    return fname(self)
    def modeldir(self):
        if self._modeldir==None: return self.basedir + self.name + '/'
        return self.basedir + self._modeldir + '/'
    def griddir(self):
	return string.split(self._modeldir,'/')[0]
    def copyModel(self,dest):
	shutil.copytree(self.modeldir(), dest+self.name)
	
    def nFiles(self):
	return len(glob.glob(self.modeldir()+'*'))
    def iofilename(self,isinput):
        return self.modeldir() + self.name + (self.outputprefix,self.inputprefix,self.errorprefix)[isinput]
    def Nscale(self):
	return 0.19347 + 0.80653*self.Z #Groves+04 change to Groves+06
    def Hescale(self):
	return 0.71553 + 0.28447*self.Z #Groves+04
    def Nascale(self):
	if True and self.hasdust and not self.dustyNotDepleted:
	    return 6.4555 #undeplete Na following Weintgartner&Draine 2001 (ApJ..563..842)
	return 1.
    def GivenHeChangeWithZ(self):
	return 0.0293 * (self.Z-1.) #Groves+04
	
    def writeInputFile(self):
	###stopping criteria
	stopstring=""
	if self.colDensity!=None:
	    if not self.neutralColumn:
		stopstring += "stop column density %f\n"%self.colDensity
	    else:
		stopstring += "stop neutral column density %f\n"%self.colDensity
	if self.stopT!=None:
	    stopstring += "stop temperature %dK linear\n"%self.stopT
	if self.isGlobule:
	    logMaxDepth = self.densityPowerLawScale + log(1-10**-(self.max_nDecs/self.densityPowerLawIndex))
	    stopstring += "stop depth %.8f\n"%logMaxDepth
	if self.AV!=None:
	    stopstring += "stop AV %.4f extended\n"%self.AV
	assert(stopstring!="")
	
	
	###SED
	SEDstrs = []
	if self.BB!=None:
	    SEDstrs.append( 'blackbody t=%f'%self.BB )
	if self.ISRF!=None:
	    SEDstrs.append( """table ism %d  
CMB"""%self.ISRF )
	if self.CRB:
	    SEDstrs.append( """COSMIC RAYS BACKGROUND""" )
	if self.HMz!=None:
	    SEDstrs.append( """table HM05 redshift %.1f"""%(self.HMz) )
	if self.starburst99fn!=None:
	    SEDstrs.append( 'table star "%s" %f'%(self.starburst99fn,self.starburst99logage) )
	if self.givenSED!=None:
	    SEDstrs.append( self.givenSED.forCloudy() )
	if self.zCMB!=None:
	    SEDstrs.append( 'CMB %.2f'%self.zCMB )
	SEDstr = string.join(SEDstrs,'\n')
	
	###intensity
	if self.luminosity!=None: 
	    #uses phi!! not Cloudy's luminosity and radius
	    intensityStr = "phi(H) %.1f\n"%log(10.**self.luminosity / (4.*pi*(10.**self.radius)**2 * self.meanhnu(self.aEUV)) )
	else:
	    if type(self.initialPressure)==type(True): 
		if self.ISRF!=None or self.HMz!=None: intensityStr = ""
		else: intensityStr = "ionization parameter %.3f\n"%self.U
	    else: 
		intensityStr = "phi(H) %.1f\n"%(self.n + self.U + u.log(3e10))
	
		    	
	###dust and metals
	if self.hasdust and (self.d2m==None or self.d2m>0.):
	    if self.d2m!=None:
		metalnames = ['he', 'li', 'be', 'b', 'c', 'n', 'o', 'f', 'ne',
		               'na', 'mg', 'al', 'si', 'p', 's', 'cl', 'ar', 'k',
		                'ca', 'sc', 'ti', 'v', 'cr', 'mn', 'fe', 'co', 'ni',
		                 'cu', 'zn']
		baseDepletions = array([1.,0.16,0.6,0.13,0.4,1.,0.6,0.3,
		                        1.,0.2,0.2,0.01,0.03,0.25,1.,0.4,
		                        1.0,0.3,0.0001,0.005,0.008,0.006,
		                        0.006,0.05,0.01,0.01,0.01,0.1,0.25])
		depletions = 1. - (1. - baseDepletions) * self.d2m
		depletionstr = string.join(['%s %s\n'%( ('abundances','continue  ')[j!=0],
		                                        string.join(['%2s=%.4f'%(metalnames[j+i], depletions[j+i]) for i in range(min(5,len(depletions)-j))]) )
		                            for j in range(0,len(depletions),5)],'')[:-1]
		metals="""%s
grains ism %f
metals and grains %f
save grains opacity "grains.coutput" last
save grains charge "grainsCharge.coutput" last
save grains drift velocity "grainsDrift.coutput" last
save grains heating "grainsHeating.coutput" last
%ssave grains temperature "grainsTemperature.coutput" last"""%(depletionstr, self.d2m, self.Z, ('','save grains qs "grainsQs.coutput" last\n')[self.savelevel>0])
	    elif self.dustyNotDepleted:
		metals="""metals and grains %f
save grains opacity "grains.coutput" last
save grains charge "grainsCharge.coutput" last
save grains drift velocity "grainsDrift.coutput" last
save grains heating "grainsHeating.coutput" last
%ssave grains temperature "grainsTemperature.coutput" last"""%(self.Z,('','save grains qs "grainsQs.coutput" last\n')[self.savelevel>0])
	    else:
		if self.dustType==None: 
		    dustStr = ''
		else:
		    dustStr = 'no grains\n'
		    for ss in string.split(self.dustType,'_'):
			dustStr += 'grains %s\n'%ss
		metals="""abundances ism %s
metals and grains %f
save grains opacity "grains.coutput" last
save grains charge "grainsCharge.coutput" last
save grains drift velocity "grainsDrift.coutput" last
save grains heating "grainsHeating.coutput" last
%ssave grains temperature "grainsTemperature.coutput" last"""%(dustStr[:-1],self.Z,('','save grains qs "grainsQs.coutput" last\n')[self.savelevel>0])
	else:
	    metals = """metals %f"""%self.Z
	
	### density, pressure, temperature, turbulence and gravity
	if type(self.initialPressure)!=type(True):
	    pressure = """constant pressure set %.1f reset
"""%self.initialPressure
	elif self.initialPressure:
	    pressure = """constant pressure // 'no abort' not needed if Pline << Pgas 
"""
	else:
	    pressure = ""
	if not self.emittedRadiationPressure:
	    pressure += """No radiation pressure
"""
	# Groves04: We therefore have chosen to set the hydrogen density in the models to be the density in the [S ii] emission
	#           zone, physically near to the ionization front, but where there are still sufficient ionizing photons to ensure
	#           that n_Hii / n_Hi ~ 1
	# Hazy:     By default the gas pressure in the first zone is derived from hden and the resulting kinetic temperature. 
        #           In consecutive iterations only T changes	
	#           If the keyword 'reset' is used, nT changes in consective iterations so the total P is constant.
	if self.inputT == None: tempstr = ""    
	else: tempstr = 'Constant temperature %.2f\n'%self.inputT
	
	if self.turbulence in (None,0): turbstr=""
	else: turbstr = "turbulence %d no pressure\n"%self.turbulence
	
	if self.densityPowerLawIndex==None:
	    densityStr = 'hden %.3f'%self.n	
	elif not self.isGlobule:
	    densityStr = 'hden %.3f, power = %.2f, scale column density = %.2f'%(self.n,self.densityPowerLawIndex,self.densityPowerLawScale)
	else:
	    densityStr = 'Globule density=%.3f, depth=%.8f, power=%.2f'%(self.n,self.densityPowerLawScale,self.densityPowerLawIndex)
	
	### calculation	    
	if self.setPresIoniz!=None: setpresioniz = """SET PRES IONIZ %d
"""%self.setPresIoniz
	else: setpresioniz=""
	if self.saveTmap: saveTmapstr = 'set nmaps %d\nsave map zone 0 range 1 to 9.5 "Tmap.coutput" \n'%self.nmaps
	else: saveTmapstr = ""
	if self.drmax!=None: drmaxStr = 'set drmax %.2f\n'%self.drmax
	else: drmaxStr = ""
	    
	
	### elements
	elements=string.join(['save element %s "%s.coutput" last'%(element.name,element.name) for element in self.elements],'\n')
	linenames = string.join([x.cloudyName for x in self.emissionLines],'\n')
	
	### input file creation
        variable="""set save prefix "%s"
%s%s%s%s
%s
%s%s%s%s%s%s%s%s%s
"""%(self.modeldir(), stopstring, intensityStr, drmaxStr,densityStr, metals, pressure, 
     "element nitrogen scale %f\n"%self.Nscale(),
     "element helium scale %f\n"%self.Hescale(),
     "element sodium scale %f\n"%self.Nascale(),
     setpresioniz,
     ("","iterate to convergence max=%d\n"%self.maxIterations)[self.iterate],
     turbstr,tempstr,SEDstr)	
        general = """print citation
print ages
save abundances "abundances.coutput" last
save ages "timescales.coutput" last
save overview "overview.coutput" last 
save radius "radius.coutput" last
save hydrogen ionization "recombination.coutput" last 
save hydrogen populations "hydrogenPopulation.coutput" last
save temperature "temperature.coutput"
save pressure "pressure.coutput" last 
// save line labels "linelabels.coutput" last
save lines, emissivity, "lines.coutput" last
%s
end of lines
save lines, emissivity, emergent "linesEmergent.coutput" last 
%s
end of lines
save heating "heating.coutput" last
save cooling "cooling.coutput" last
%s%ssave ionizing continuum every "ionizingContinuum.coutput" last  
save transmitted continuum "transmittedSpectrum.coutput" last  
%s%s"""%(linenames, linenames,
       ('','save continuum "cloudEmission.coutput" last\nsave lines, array "lineTotals.coutput" last\n')[self.savelevel>0],
       ('save total opacity "opacity.coutput" last\n','save continuum every "ionizingSpectrum.coutput" last\nsave total opacity every "opacity.coutput" last\n')[self.savelevel>1],
       saveTmapstr,elements)
        f = file(self.iofilename(isinput=True),'w')
        f.write(variable + general)
        f.close()
	self.CloudyStat="files written"
    
    ## running and verifying run
    def run(self,showprogress=False):
	runModel(self,showprogress=showprogress)	
    def CloudyOk(self):
	"""
	to check warnings run in model dir:
	grep "W-" */<model name>*coutput
	"""
	try: 
	    f = file(self.iofilename(isinput=False))
	except IOError:
	    return False
	l = f.readlines()[-1]
	f.close()
	return 'Cloudy exited OK' in l

    def nIterations(self):
	ls = u.filelines(self.iofilename(isinput=False))
	ss = string.split(ls[-2])
	ind = [i for i in rl(ss) if 'iterations' in ss[i]][0]
	return float(ss[ind-1])	
    def stopReason(self,maxIters=100):
	ls = u.filelines(self.iofilename(isinput=False))
	reasonStrs = [None]*maxIters
	for l in ls: 
	    if 'Calculation stopped because' not in l: continue
	    ind = l.index(' Iteration')
	    reasonStr = l[31:ind]
	    iterNumber= int(l[ind+11:ind+13])
	    reasonStrs[iterNumber-1] = reasonStr
	return reasonStrs[:reasonStrs.index(None)]
    def lastStopReason(self,maxIters=100):
	return self.stopReason(maxIters)[-1]
    
    ## loading results
    def readOutputFile(self, filename, lineprefix='',fields=False,fieldstart=64, fieldsize=32,vallen=9, retBreaks=False,turnToFloat=True,tillfirstbreak=False):
        f = file(self.modeldir() + filename + self.outputprefix)
        ls = []; breaks = []; i=0
        while True:
            l = f.readline()
            if l=='': break
            if l[0]!='#' and l[0]!='\n':
                if ( (type(lineprefix)==type('') and l[:len(lineprefix)]==lineprefix) or
                     (type(lineprefix)==type(()) and lineprefix[0]<float(l.split(None,1)[0])<lineprefix[1]) ) :
                    ls.append(l)
                    i+=1
            else:
		if not tillfirstbreak: breaks.append(i)                
		elif i>0: break
        f.close()
        if not fields:
            vals = [[(str,u.floatCanFail)[turnToFloat](x) for x in string.split(l)] for l in ls]
        else:
            vals = []
            for l in ls:
                fields = [l.expandtabs()[i:i+fieldsize].rstrip() for i in range(fieldstart,len(l.expandtabs())-1,fieldsize)]
                vals.append( dict([(field[:-vallen].rstrip(),float(field[-vallen:])) for field in fields]) )
        if not retBreaks: return vals
        else: return vals, breaks
    def loadAll(self,requireNoWarnings=True,loadTransmitted=False):	
	if requireNoWarnings and not self.CloudyOk(): return
	self.loadDepths()
	if self.savelevel>0: 
	    self.loadInOutSpectrum()
	else:
	    self.loadSpectrumEnergies()
        self.loadOverview()
        self.loadIonization()
        self.loadEnergetics()
        self.loadElements()
        self.loadLines()
	if self.savelevel>1: 
	    self.loadOpacities(onlyIonizing=True)  #no real reason for this value of onlyIonizing, just backward compatibility
	else:
	    self.loadOpacities(onlyIonizing=False)
        if self.savelevel>1: 	    
	    self.loadSpectra()  
	self.loadPressure()
	self.loadHydrogen()
	self.loadAbundances()
	if loadTransmitted: self.loadTransmitted()
	self.loaded = True
	self.CloudyStat = "loaded"
    def loadTmap(self,filename = 'Tmap',returnEquilibriumTemperature=False,includeIonization=False):
	assert(self.saveTmap)
	ls = u.filelines(self.modeldir() + filename + self.outputprefix)
	Hdic = {}
	Cdic = {}
	if includeIonization:
	    n_e = {}
	    n_HI = {}
	    n_HeI = {}
	    n_HeII = {}
	for l in ls:
	    try: T = float(l[1:11])
	    except: continue
	    Hdic[T] = float(l[12:22])
	    Cdic[T] = float(l[38:48])
	    if includeIonization:
		ss = string.split(l)
		n_e[T] = float(ss[-6])
		n_HI[T] = float(ss[-5]) * 10**float(ss[-4])
		n_HeI[T] = float(ss[-5]) * self.Hescale() * 10**float(ss[-3])
		n_HeII[T] = float(ss[-5]) * self.Hescale() * 10**float(ss[-2])
	if returnEquilibriumTemperature:
	    Teq = float(string.split(ls[1])[3])
	    return Hdic, Cdic, Teq
	if includeIonization:
	    return Hdic, Cdic, n_e, n_HI, n_HeI, n_HeII
	return Hdic, Cdic
    def loadSpectrumEnergies(self, energyrange=(1e-8,1e6),filename = 'transmittedSpectrum'):
	vals = self.readOutputFile(filename)[6:]
	s,e = searchsorted([x[0] for x in vals], energyrange)
	self.spectrumEnergies = array([x[0] for x in vals[s:e]])
    def loadInOutSpectrum(self, energyrange=(1e-8,1e6),filename = 'cloudEmission'):
        """
        header: energy nFn
        """
        vals = self.readOutputFile(filename)
        s,e = searchsorted([x[0] for x in vals], energyrange)
        self.spectrumEnergies = array([x[0] for x in vals[s:e]])
        self.incidentSpectrumnuFnus   = array([x[1] for x in vals[s:e]])
	self.observedSpectrumnuFnus   = array([x[5] for x in vals[s:e]])
	self.emittedOutwardSpectrumnuFnus   = array([x[3] for x in vals[s:e]])
	self.attenuatedAndDiffuse = array([x[4] for x in vals[s:e]])
	self.attenuated = array([x[2] for x in vals[s:e]])
	self.emittedReflectedAndemittedOutwardandAttentuatedSpectrumnuFnus   = array([x[6] for x in vals[s:e]])
	self.lineSpectrumnuFnus   = array([x[7] for x in vals[s:e]])  #only reflected emission
	self.lineOutwardSpectrumnuFnus   = array([x[8] for x in vals[s:e]])
    def loadDepths(self, filename = 'radius'):
	vals = self.readOutputFile(filename)
	self.depths    = array([sum([vals[i][3] for i in range(j)])+vals[j][3]/2. for j in rl(vals)])
	self.olddepths = array([vals[j][2] for j in rl(vals)]) 
	self.ddepths   = array([vals[j][3] for j in rl(vals)]) 
    def loadOverview(self,filename = 'overview'):
        """
        header: depth Te Htot hden eden 2H_2/H HI HII HeI HeII HeIII CO/C C1 C2 C3 C4 O1 O2 O3 O4 O5 O6 AV(point) AV(extend)
        """
        vals = self.readOutputFile(filename)
        self.Ts, self.heating, self.nH, self.ne = [array([x[i] for x in vals]) for i in (1,2,3,4)]
        self.Hstate, self.Hestate, self.Ostate = [[array([x[i] for x in vals]) for i in iStates] for iStates in [(6,7),(8,9,10),(16,17,18,19,20,21)]]
        self.AVs = [[x[i] for x in vals] for i in (22,23)]
    def loadElements(self):
	self.elementStates = {}
        for iElement,element in enumerate(self.elements):
	    elementLetter = element.shortname()
	    try:
		vals = self.readOutputFile(element.name)
	    except IOError:
		continue
	    self.elementStates[elementLetter] = [array([log(x[i]) for x in vals]) for i in range(1,element.nElectrons()+2)]
	self.elementStates['H'], self.elementStates['He'] = self.Hstate, self.Hestate
	    
    def loadIonization(self, filename = 'recombination'):
        vals = self.readOutputFile(filename,lineprefix='hion')
        self.ionizationRates, self.recombinationCoeffsTotal, self.recombinationCoeffsB =  [[x[i] for x in vals] for i in (2,4,5)]        
    def loadLines(self, filenames = ('lines','lineTotals')):
	vals = self.readOutputFile(filenames[0])
	self.linesdicF = dict([(self.emissionLines[i].key(), array([x[i+1] for x in vals])) for i in rl(vals[0],-1)])
	vals = self.readOutputFile(filenames[0] + 'Emergent')
	self.linesdicE = dict([(self.emissionLines[i].key(), array([x[i+1] for x in vals])) for i in rl(vals[0],-1)])
	
	if self.savelevel>0:    
	    vals = self.readOutputFile(filenames[1],turnToFloat=False)
	    linesdic = dict([(string.join(val[1:-3]), float(val[-2])) for val in vals])
	    self.linesdicT = dict([(self.emissionLines[i].key(), linesdic.get(self.emissionLines[i].upperCaseName(),None)) for i in rl(self.emissionLines)])
    def loadAdditionalLines(self,ks,filename='lineTotals'):
	vals = self.readOutputFile(filename,turnToFloat=False)
	linesdic = dict([(string.join(val[1:-3]), float(val[-2])) for val in vals])
	self.linesdicT.update( dict([(k, linesdic.get(k,None)) for k in ks]) )
    def loadAllLines(self,filename='lineTotals'):
	vals = self.readOutputFile(filename,turnToFloat=False)
	linelist = [CloudyLineOutput(float(v[0]), string.join(v[1:-3]), 10.**float(v[-3]), 10**float(v[-2]), v[-1]) for v in vals]
	self.allLinesDic = dict([(l.cloudyname,l) for l in linelist])
    
    def lineEW(self,wl,lineFlux,CF_line,useR06,basewl = 1450.,CF_continuum=1.):
	""" this function is correct only for slab geometry"""
	if useR06:
	    ind =  u.searchsorted(self.nus(False),3e18/basewl)
	    nuFnu_unit_CF = self.incidentSpectrumnuFnus[ind] * 10**(tf.R06LBolFac(basewl) - tf.R06LBolFac(wl)) 
	else:
	    ind =  u.searchsorted(self.nus(False),3e18/wl)
	    nuFnu_unit_CF = self.incidentSpectrumnuFnus[ind] 
	Llambda = nuFnu_unit_CF / wl * CF_continuum 
	return lineFlux * CF_line / Llambda
    def AllStrongLines(self,minwl,maxwl,minEW,CF_line,useR06,CF_continuum):
	lines = self.loadAllLines()
	return [l for l in lines if minwl < l.wl < maxwl and self.lineEW(l.wl, l.emergentI, CF_line,useR06,CF_continuum=CF_continuum)>minEW]

    def loadEnergetics(self, filenames = ('heating','cooling')):
	self.totalHeating = array([x[2] for x in self.readOutputFile(filenames[1],fields=False)])
	self.totalCooling = array([x[3] for x in self.readOutputFile(filenames[1],fields=False)])
	#self.heaters  = self.readOutputFile(filenames[0],fields=True,fieldsize=24)	    ##commented out due to bug in output
	#self.coolants = self.readOutputFile(filenames[1],fields=True,fieldsize=32)         ##commented out due to bug in output
	    
    def loadAbundances(self):
	ls = u.filelines(self.iofilename(False))
	ind = [il for il in rl(ls) if ls[il].strip()=='Gas Phase Chemical Composition'][0]
	abundls = [l.strip() for l in ls[ind+1:ind+5]]
	abundstrs = u.sumlst([[l[13*i:13*i+11] for i in range(len(l)/13+1)] for l in abundls])
	abundstrsplit = [string.split(a,':') for a in abundstrs]
	self.abundances = dict([(a[0].strip(),10**float(a[1])) for a in abundstrsplit if len(a)==2])
    def abundance2Solar(self,el):
	return self.abundances[el] / elementListDic[el].abundance()
    def loadOpacities(self, filenames = ('opacity','grains'),onlyIonizing=True):
	if self.savelevel>1: 
	    vals, breaks = self.readOutputFile(filenames[0],lineprefix=((1e-8,1e6),self.ionizationSpectrumLoadEnergies)[onlyIonizing],retBreaks=True)
	
	    tmp = [array([x[1] for x in vals[breaks[i]:breaks[i+1]]]) for i in rl(breaks,-1)][:-1]  #there is one too many total opacities
	    if onlyIonizing:    self.totalKappas = tmp
	    else:               self.allTotalKappas = tmp
	    
	    vals = self.readOutputFile(filenames[0],tillfirstbreak=True)
	    self.faceTotalOpacities = array([x[1] for x in vals[:len(self.spectrumEnergies)]]) / 10**self.nH[0]
	else:	  
	    try: #for backward compatibility
		vals = self.readOutputFile(filenames[0])
		tmp = array([x[1] for x in vals[:len(self.spectrumEnergies)]]) #radiation pressure cross section	    
		if onlyIonizing:    self.totalKappas = tmp
		else:               self.allTotalKappas = tmp	    
	    except:
		pass
	if self.hasdust:
	    vals = self.readOutputFile(filenames[1])
	    self.grainOpacities = array([x[1] for x in vals[:len(self.spectrumEnergies)]]) 
    def loadGrainTemperatures(self, filename = 'grainsTemperature'):
	assert(self.hasdust)
	vals = self.readOutputFile(filename)
	self.grainTemperatures = array(vals)[:,1:]
	
    def totalOpacities(self,onlyIonizing=True):
	if onlyIonizing: kappas = self.totalKappas
	else: kappas = self.allTotalKappas
	return [kappas[i] / 10**self.nH[i] for i in range(self.nZones())]
    def totalOpacitiesSingleZone(self,iZone,onlyIonizing=True):
	if onlyIonizing: kappas = self.totalKappas
	else: kappas = self.allTotalKappas
	if self.savelevel<2: #only opacities of last zone are saved
	    return kappas / 10**self.nH[-1]
	else:
	    return kappas[iZone] / 10**self.nH[iZone]
    
    def gasOpacities(self,onlyIonizing=True):
	if onlyIonizing:
	    s,e = self.ionizationEnergyInds()
	    return [self.totalOpacities(True)[i]-self.grainOpacities[s:e] for i in range(self.nZones())]
	else:
	    return [self.totalOpacities(False)[i]-self.grainOpacities for i in range(self.nZones())]
    def gasOpacitiesSingleZone(self,iZone,onlyIonizing=True):
	if onlyIonizing:
	    s,e = self.ionizationEnergyInds()
	    return self.totalOpacitiesSingleZone(iZone,True)-self.grainOpacities[s:e]
	else:
	    return self.totalOpacitiesSingleZone(iZone,False)-self.grainOpacities
	
    def Qnus(self,i):	
	h = 6.62606957e-27	
	return self.spectra[i] / (h*self.nus()**2)
    def Kappas(self,elementname='H',ionlevel=0):
	nus0    = self.nus0dic[(elementname, ionlevel)]
	nus0ind = searchsorted(self.nus(),nus0)	
	
	nIonPernH    = 10**getattr(self,elementname + 'state')[ionlevel] / 10**self.nH
	nu0Opac = self.nus0Opacdic[(elementname, ionlevel)]
	opacGas = nu0Opac * (self.nus()/nus0)**(-3.)
	
	s,e = self.ionizationEnergyInds()
	opacDust = self.grainOpacities[s:e]
	
	return array([nIonPernH[i] * (self.Qnus(i)*opacGas* (nIonPernH[i]*opacGas / (nIonPernH[i]*opacGas + opacDust)) *self.dnus())[nus0ind:].sum() /
	              (self.Qnus(i)*self.dnus())[nus0ind:].sum() for i in rl(self.spectra)])
    def meanSigmaDust(self,ionizing=False,photonmean=False,power=1.,grainOpacities=None): #left for backward compatibility
	return self.meanSigma(iZone=0,component='dust', ionizing=ionizing,photonmean=photonmean,power=power,inputOpacities=grainOpacities)
    def meanSigma(self,iZone=0,component=None,ionizing=False,photonmean=False,power=1.,inputOpacities=None,onlyOptical=False,spectrum=None):
	if ionizing:
	    s,e = self.ionizationEnergyInds()
	elif onlyOptical:
	    s,e = 1,self.ionizationEnergyInds()[0]	    
	else:
	    s,e = 1,-1
	if inputOpacities!=None:
	    opacities = inputOpacities
	elif component=='dust':
	    opacities = self.grainOpacities[s:e]
	elif component=='gas':
	    opacities = self.gasOpacitiesSingleZone(iZone,False)[s:e]
	normpower = (1,2)[photonmean]
	if spectrum!=None:
	    spec = spectrum[s:e]
	elif self.savelevel==1:
	    spec = self.incidentSpectrumnuFnus[s:e]
	elif self.savelevel>=2:
	    if ionizing:
		spec = self.spectra[iZone]
	    else:
		spec = self.allSpectra[iZone][s:e]
	totalOpac = ((spec / self.nus(False)[s:e]**normpower * opacities**power) * self.dnus(False)[s-1:e-1]).sum()	    
	denom     = ((spec / self.nus(False)[s:e]**normpower)                    * self.dnus(False)[s-1:e-1]).sum()	    
	return totalOpac / denom
    
    
    def ionizationEnergyInds(self):
	return [searchsortedclosest(self.spectrumEnergies, energy) for energy in self.ionizationSpectrumLoadEnergies]
    def ionizationEnergies(self):
	s,e = self.ionizationEnergyInds()
	return self.spectrumEnergies[s:e]
    def nus(self,ionizing=True):
	if ionizing: es = self.ionizationEnergies()
	else: es = self.spectrumEnergies
	return 3e18/u.rydberg2Angstrom(es)
    def dnus(self,ionizing=True,ionizingShort=False):
	if ionizing: 
	    if ionizingShort:
		s,e = self.ionizationEnergyInds()	    
	    else:
		s,e = self.ionizationEnergyInds()[0]-1, self.ionizationEnergyInds()[1]+1	
	else: 
	    s,e = None, None
	nus = 3e18/u.rydberg2Angstrom(self.spectrumEnergies[s:e])
	return (nus[2:] - nus[:-2])/2.	
    def loadSpectra(self, filename = 'ionizingSpectrum',onlyIonizing=True):
        vals, breaks = self.readOutputFile(filename,lineprefix=((1e-8,1e6),self.ionizationSpectrumLoadEnergies)[onlyIonizing],retBreaks=True)
        breaks += [len(vals)]
	tmp = array([array([x[2] for x in vals[breaks[i]:breaks[i+1]]]) for i in rl(breaks,-1)])
	if onlyIonizing: self.spectra = tmp
	else:            self.allSpectra = tmp
	self.diffuseSpectra = array([array([x[3] for x in vals[breaks[i]:breaks[i+1]]]) for i in rl(breaks,-1)])
	self.transmittedSpectra = array([array([x[4] for x in vals[breaks[i]:breaks[i+1]]]) for i in rl(breaks,-1)])
	self.emittedspectra = array([array([x[5] for x in vals[breaks[i]:breaks[i+1]]]) for i in rl(breaks,-1)])
    def loadTransmitted(self, filename = "transmittedSpectrum"):
	vals = self.readOutputFile(filename)[6:]
	self.transmittedSpectrum = array([x[1] for x in vals])[:len(self.spectrumEnergies)]
    def loadPressure(self, filename = 'pressure'):
	vals = self.readOutputFile(filename)
	self.Pcorrect, self.Pcurrent, self.Ptotal = [array([x[i] for x in vals]) for i in (1,2,3)] #all these should be the same
	self.Pgas, self.Prad = [array([x[i] for x in vals]) for i in (5,8)]
    def loadHydrogen(self, filename = 'hydrogenPopulation'):
	vals = self.readOutputFile(filename)
	self.nHI, self.nHII, self.nH1s, self.nH2s, self.nH2p, self.nH3 = [array([x[i] for x in vals]) for i in range(1,7)]
	
    def localColumnDensity(self, attr='nH',islog=True, index=None,k=None,fractional=False):	
	vals = getattr(self, attr) 
	if k!=None: vals = vals[k]
	if index!=None: vals = vals[index]
	if islog: vals = 10.**vals
	if fractional!=False: vals*=10**self.nH*fractional
	dcolumn = vals * self.ddepths
	return array([dcolumn[:i+1].sum() for i in rl(dcolumn)])
    def Nion(self,ion):
	if ion.__class__!= Ion: ion = Ion(ion)
	return self.localColumnDensity('elementStates',index=ion.ionLevel,k=str(ion.el),fractional=self.abundances[str(ion.el)])
    def Nelement(self,el):
	return (self.Nion('HI')+self.Nion('HII')) * self.abundances[str(el)]
    def getAllEnergyProcesses(self, iscoolant=True):
        lst = (self.heaters, self.coolants)[iscoolant]
        return sorted(unique(concat([d.keys() for d in lst])).tolist())
    def getEnergyProcess(self, processname,iscoolant=True):
        lst = (self.heaters, self.coolants)[iscoolant]
        return array([d.get(processname,0) for d in lst])
    def printEnergySummary(self,iscoolant=True):
        out(['%s\t %.3f'%y for y in 
             sorted([(k,self.getEnergyProcess(k,iscoolant).max()) 
                     for k in self.getAllEnergyProcesses(iscoolant)],key=lambda x: -x[1])])
    def getMainCoolants(self, zone,n=1):
	items = self.coolants[zone].items()
	sitems = sorted(items,key=lambda x: -x[1])
	return [x[0] for x in sitems[:n]]
    def nZones(self):
	return len(self.nH)
    def zoneByNHI(self,lNHI):
	return searchsorted(self.localColumnDensity('Hstate',index=0,fractional=1.),10**lNHI)
    def incidentFlux(self):
	return ((self.incidentSpectrumnuFnus / self.nus(False) )[1:-1] * self.dnus(False)).sum()
    def incidentIonizingFlux(self):
	s,e = self.ionizationEnergyInds()
	return ((self.incidentSpectrumnuFnus[s:e] / self.nus() ) * self.dnus()).sum()
    def incidentPhotonFlux(self):
	h = 6.62606957e-27	    
	s,e = self.ionizationEnergyInds()
	return ((self.incidentSpectrumnuFnus[s:e] / (h * self.nus()**2) ) * self.dnus()).sum()
    
    def getBandLuminosity(self,maxwl,minwl,zone=None,withLines=True,includeIncident=True):
	
	if zone==None:
	    rydberg = 1/1.0968e5 #cm	    
	    s,e = [searchsortedclosest(self.spectrumEnergies, rydberg*1e8 / wl) for wl in maxwl,minwl]
	    if includeIncident: spec = self.observedSpectrumnuFnus
	    else: spec = self.emittedSpectrumnuFnus
	    total = ((spec / self.nus(False) )[s+1:e+1] * self.dnus(False)[s:e]).sum()
	    #if self.radius!=None and self.luminosity!=None: total *= 4.*pi*(10.**self.radius)**2.  #in this case a default CF=1 is assumed
	    if not withLines:
		totalLines = ((self.lineSpectrumnuFnus / self.nus(False) )[s+1:e+1] * self.dnus(False)[s:e]).sum()
		total -=totalLines
	    return total	
	else:
	    s,e = [searchsortedclosest(self.nus(), 3e18 / wl) for wl in maxwl,minwl]
	    dL = (((self.emittedspectra[zone] - (self.emittedspectra[zone-1],0.)[zone==0]) / self.nus() )[s:e] * self.dnus()[s:e]).sum()
	    return dL / self.ddepths[zone]
    def AMD(self,dlogxi=0.25,xirange=(-5,8),getUs = False, phiCalculatedAtSurface=False):
	if not getUs:
	    xis = u.log(self.localxi())
	else:
	    if phiCalculatedAtSurface:
		xis = u.log(self.incidentPhotonFlux() / 3e10 / 10**self.nH)
	    else:
		xis = u.log(self.localU())
	dNs = self.ddepths * 10**self.nH
	inds = rl(self.nH)
	xs = u.arange(xirange[0]+dlogxi/2., xirange[1], dlogxi)
	ys = zeros(len(xs))
	for ind in inds:
	    xsind = searchsortedclosest(xs, xis[ind])
	    ys[xsind] += dNs[ind]
	ys = array([(u.log(ys[i] / dlogxi), 0.)[ys[i]==0.] for i in rl(ys)]) 
	xs2 = mergearrs([xs-dlogxi/2., xs+dlogxi/2.])
	ys2 = mergearrs([ys,ys])
	s,e = ys2.nonzero()[0][0],ys2.nonzero()[0][-1]
	init = ys2[e]
	return xs2[s:e+1],ys2[s:e+1]
    def atNion(self,ion,N,func,*args,**kwargs):
	vals = func(self,*args,**kwargs)
	return u.searchAndInterpolate(self.Nion(ion), N, vals)#if val is None? 

def runModel(model,showprogress=False):  #not an instance method because instance method can't be pickled
    #processinfo('runModel()')
    stdin  = file(model.iofilename(isinput=True ), 'r')
    stdout = file(model.iofilename(isinput=False), 'w')
    stderr = file(model.iofilename(isinput=2), 'w')
    print 'running: %s'%model.name, 'process id:', os.getpid()	
    proc = subprocess.call(model.cloudyexe, stdout=stdout, stdin=stdin,stderr=stderr)
    stdin.close()
    stdout.close()
    print 'ending: %s'%model.name, 'process id:', os.getpid()
    return True    
def loadModel(model,requireNoWarnings,loadTransmitted):
    print 'running: %s'%model.name, 'process id:', os.getpid()	
    #print model    
    model.loadAll(requireNoWarnings,loadTransmitted=loadTransmitted)
    print 'ending: %s'%model.name, 'process id:', os.getpid()	
    
    

class Grid(dict):
    def tostr(self, char, val,nDigitsDic={}):
	ndigits = nDigitsDic.get(char,1)
	if type(val)==type(''): 
	    if self.strfunc==None: val = float(val[-4:])
	    else: return self.strfunc(val)
	if isinstance(val, SED): return '_%c%s'%(char,self.SEDstrfunc(val))
	if val!=None: return ('_%c%.'+str(ndigits)+'f')%(char,val)
	else: return ''
    def tokey(self, k):
	if isinstance(k,SED): return self.SEDkeyfunc(k)
	return k
    def __init__(self, gridname, ns=[None], Us=[None], Zs=[None],ps=[True],SEDs=[None],colDenss=[None],
                 neutralColumn=False,setPresIoniz=None,iterate=True,strfunc=None,
                 stopT=None,electronTemps=[None],AVs=[None],
                 notCartesian=[],forCompatibility=[],hasdust=True,dusties=[None],
                 emittedRadiationPressure=True,HMzs=[None],                 
                 Ts=[None],d2ms=[None],turbulences=[None],ISRFs=[None],emissionLines=None,
                 SEDstrfunc=None,SEDkeyfunc=None,nDigitsDic={},
                 starburst99fns=[None],starburst99logages=[None],ionizationSpectrumLoadEnergies=(1.,1000.),
                 luminosities=[None],radii=[None],savelevel=2,maxIterations=10,CRB=False,
                 saveTmap=False,
                 densityPowerLawScales=[None],densityPowerLawIndices=[None],densityPowerLawGivenAsAMD=True,
                 drmax=None,keyfunc=None,
                 dustyNotDepleted=False,isGlobule=False,max_nDecs=7,zCMB=None,nmaps=17,dustType=[None]):
	"""
	to add a new variable which changes between the models in the grid:
	1. add it to Model as a new input to __init__ with default None
	2. add functionality in Cloudy Model
	3. add it to Grid.__init__ as a new input with default [None]
	4. save it as a member in Grid
	5. add it to variables list below: (variable, "<cloudyModel" variable, first letter)
	"""
        dict.__init__(self)
	self.strfunc = strfunc
	self.keyfunc = keyfunc
	self.SEDkeyfunc = SEDkeyfunc
	self.SEDstrfunc = SEDstrfunc
	self.ns = ns
	self.Us = Us
	self.Zs = Zs
	self.electronTemps = electronTemps
        self.name = gridname
	self.ps = ps	
	self.SEDs=SEDs	
	self.radii = radii
	self.luminosities = luminosities
	if dusties==[None]: dusties = [hasdust]  ##hasdust kept for backward compatibility
	self.dusties = dusties
	sedtuple = (SEDs, 'givenSED', 'S')
	self.colDenss = colDenss
	self.AVs = AVs
	self.Ts = Ts
	self.d2ms = d2ms
	self.turbulences = turbulences	
	self.ISRFs = ISRFs
	self.HMzs = HMzs
	self.starburst99fns,self.starburst99logages = starburst99fns,starburst99logages
	self.dustType = dustType
	self.densityPowerLawIndices = densityPowerLawIndices   #note these could be in AMD format or density power law format, depending on densityPowerLawGivenAsAMD
	self.densityPowerLawScales = densityPowerLawScales     #note these could be in AMD format or density power law format, depending on densityPowerLawGivenAsAMD
        if not os.path.exists(Model.basedir + self.name): os.mkdir(Model.basedir + self.name)
        self.models = {}
	variables = [(ns,'n','n'), 
	             (Us,'U','U'), 
	             (Zs, 'Z', 'Z'), 
	             (ps, 'initialPressure', 'p'),
	             sedtuple,
	             (Ts, 'BB', 'T'),
	             (electronTemps, 'electronTemp','t'),
	             (dusties,'dust','D'),
	             (d2ms, 'd2m', 'd'),
	             (colDenss, 'columndensity', 'N'),
	             (turbulences,'turbulence','b'),
	             (ISRFs,'ISRF','i'),
	             (HMzs,'HMz','H'),
	             (starburst99fns,'starburst99fn','g'),
	             (starburst99logages, 'starburst99logage', 'h'),
	             (luminosities, 'luminosity', 'L'),
	             (radii, 'radius','r'),
	             (densityPowerLawIndices,'densityPowerLawIndex','j'),
	             (densityPowerLawScales,'densityPowerLawScale','k'),
	             (AVs,'AV','A'),
	             (dustType,'dustType','O')
	             ]
	variables = sorted(variables, key=lambda x: x[2] not in notCartesian)
	
	if len(notCartesian)>1: 
	    vals = [x[0] for x in variables if x[2] in notCartesian]
	    assert(len(u.mifkad(u.lens(vals)))==1)
	    keys = unzip(vals)
	else: keys = None
	keys = u.cartesianMultiplication([x[0] for x in variables if x[2] not in notCartesian], init=keys)
	inputnames = [x[1] for x in variables]	
	modelnames = [x[2] for x in variables]
	for k in keys:
	    modelname = gridname+string.join([self.tostr(modelnames[i],k[i],nDigitsDic) for i in rl(k) if len(variables[i][0])>1 or (modelnames[i] in forCompatibility)],'')
	    kwargs = dict([(inputnames[i], k[i]) for i in rl(k)])
	    dicKey = tuple([self.tokey(k[i]) for i in rl(k) if len(variables[i][0])>1 or variables[i][2] in forCompatibility])  
	    self[dicKey] = Model(modelname,modeldir=self.name+'/'+modelname,
	                         setPresIoniz=setPresIoniz,emissionLines=emissionLines,
	                         emittedRadiationPressure=emittedRadiationPressure,stopT=stopT,isGlobule=isGlobule,max_nDecs=max_nDecs,
	                         iterate=iterate,CRB=CRB,dustyNotDepleted=dustyNotDepleted,zCMB=zCMB,
	                         savelevel=savelevel,maxIterations=maxIterations,neutralColumn=neutralColumn,
	                         ionizationSpectrumLoadEnergies=ionizationSpectrumLoadEnergies,
	                         saveTmap=saveTmap,densityPowerLawGivenAsAMD=densityPowerLawGivenAsAMD,drmax=drmax,       
	                         nmaps=nmaps,**kwargs)
	self.keynames = [x[1] for x in variables if len(x[0])>1]
	
    def writeinputfiles(self):
        [self[k].writeInputFile() for k in self]
    def loadAll(self, requireNoWarnings=True, loadTransmitted=False, multipleProcs=7,cumulative=False):
	for k in u.Progress(self):
	    if not self[k].loaded or not cumulative:
		self[k].loadAll(requireNoWarnings=requireNoWarnings,loadTransmitted=loadTransmitted)
    def run(self, multipleProcs=7,sortFunc = None,filterFunc=u.noFilter):	    
	u.processinfo('Grid.run()')
	pool = multiprocessing.Pool(processes=multipleProcs,maxtasksperchild=1)
	skeys = filter(filterFunc, self.keys())
	print '# of models to run:', len(skeys)
	if sortFunc!=None: skeys = sorted(skeys, key=sortFunc)
	for k in skeys:
	    self[k].CloudyStat = "currently running"
	    pool.apply_async(runModel, (self[k],))
	return pool
    def __call__(self,fname,requireNoWarnings=False,*args,**kwargs):
	vals = u.myDict([(k,self[k](fname,*args,**kwargs)) for k in u.Progress(self.keys()) if ((not requireNoWarnings) or self[k].CloudyOk())])
	return vals    
    def subdiccall(self,subdicfunc,fname,*args,**kwargs):
	return u.myDict([(k,self[k](fname,*args,**kwargs)) for k in self.keys() if subdicfunc(k)])
    def keys(self):
	ks = sorted(dict.keys(self))
	if len(ks[0])==1: return [x[0] for x in ks]
	return ks
    def keydict(self,k):
	d = dict([(self.keynames[i], k[i]) for i in rl(k)])
	for singlevals,name in (self.ns,'n'), (self.Us,'U'), (self.aEUVs,'aEUV'), (self.Zs,'Z'), (self.ps,'p'), (self.SEDs,'inputSEDdir'):
	    if len(singlevals)==1: 
		d[name] = singlevals[0]
	return d
    #def __getitem__(self,k):
	#if type(k)==type(()): return dict.__getitem__(self,k)
	#if type(k)==type({}):
    def CloudyStatus(self):
	out([(k,self[k].CloudyStat) for k in self])
    def __getitem__(self,k):
	if type(k)==type((None,)):
	    return dict.__getitem__(self,k)
	else:
	    return dict.__getitem__(self,(k,))
    def dirFiles(self):
	return u.mifkad(self('nFiles').values())
	


    def addGrid(self, other,joinOnAttr):
	"""assumes single value in joinOnAttr"""
	selfAttr = getattr(self,joinOnAttr)[0]
	otherAttr = getattr(other,joinOnAttr)[0]
	setattr(self,joinOnAttr,[selfAttr,otherAttr])
	for k in self.keys():
	    newk = tuple([selfAttr]+list(k))
	    self[newk] = self[k]
	    self.pop(k)
	for k in other.keys():
	    newk = tuple([otherAttr]+list(k))
	    self[newk] = other[k]
    
    def griddir(self):
	return self.values()[0].griddir()
    def copyGrid(self,dest):
	os.mkdir(dest + self.griddir())
	self('copyModel',False,dest + self.griddir() + '/')
    def createLookupDic(self,ions,lnHs = arange(-8.,1,.1),minNion = 1e-10):   
	outfile = file(Model.basedir + self.griddir() + '/predictedNions.txt','w')
	outfile.write('ion  \tindex\tscale\tn_0  \tn_max\tNion\n')
	lNionsDic = {}
	for ion in u.Progress(ions):
	    lNionsDic[str(ion)] = zeros((len(lnHs),
	                                 len(self.densityPowerLawIndices),
	                                 len(self.densityPowerLawScales),
	                                 len(self.Zs),
	                                 len(self.ns)))
	    for iIndex,index in enumerate(self.densityPowerLawIndices):
		for iScale,scale in enumerate(self.densityPowerLawScales):
		    for i_n0, ln0 in enumerate(self.ns):
			for iZ, Z in enumerate(self.Zs):
			    m = self[self.keyfunc(index,scale,Z,ln0)]
			    model_lnHs = m.nH
			    model_lNions = log(u.maxarray(m.Nion(str(ion)),minNion))
			    for inH,lnH in enumerate(lnHs):
				if lnH<model_lnHs[0] or lnH>model_lnHs[-1]:
				    val = nan
				else:				
				    val = u.searchAndInterpolate1D(model_lnHs,lnH,model_lNions) 
				lNionsDic[str(ion)][inH,iIndex,iScale,iZ,i_n0] = val
				outfile.write('%5s\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\t%.2f\n'%(ion,index,scale,Z,ln0,lnH,val))
	outfile.close()
				
	return lNionsDic


  
LineNames  = [
    (('h  1 4861',), {'label':r'$H\beta$'}),
    (('o  3 5007',), {'ncrit': 5.8}),
    (('o  1 6300',), {'ncrit': 6.3}),
    (('n  2 6584',), {'ncrit': 4.9}),
    (('h  1 6563',), {'label': r'$H\alpha$'}),
    (('s II 6716',), {'ncrit': 3.2,'transitionProb':2.6e-4,'colStrength':6.9*3.,'statWeights':(3,2)}),
    (('s II 6731',), {'ncrit': 3.6,'transitionProb':8.8e-4,'colStrength':6.9*2.,'statWeights':(2,2)}),
    (('ne 5 3426',), {'ncrit': 7.3}),
    (('ne 5 3346',), {'ncrit': 7.3}),
    (('totl 3727', 'O', 2), {'ncrit': 3.15}), 
    (('ca a 3750', 'H', 1), {}),
    (('ca b 3750', 'H', 1), {}),
    (('ca a 3771', 'H', 1), {}),
    (('ca b 3771', 'H', 1), {}),
    (('h  1 3798',), {}),
    (('h  1 3835',), {}),
    (('ne 3 3869',), {'ncrit': 7.0}),
    (('h  1 3889',), {}),
    (('h  1 3970',), {}),
    (('s II 4070',), {'ncrit': 6.4}),
    (('h  1 4102',), {}),
    (('h  1 4340',), {}),
    (('totl 4363', 'O', 3), {'ncrit': 7.5}),
    (('he 1 4471',), {}),
    (('he 2 4686',), {}),
    (('ar 4 4711',), {'ncrit': 4.4}),
    (('totl 5199', 'Na', 1), {'ncrit': 3.3}),
    (('n  2 5755',), {'ncrit': 7.5}),
    (('he 1 5876',), {}),
    (('na 1 5892',), {}),
    (('fe 7 6087',), {'ncrit': 7.6}),
    (('he 1 6678',), {}),
    (('ar 3 7135',), {'ncrit': 6.7}),
    (('o II 7323',), {'ncrit': 6.8}),
    (('o II 7332',), {'ncrit': 6.8}),
    (('ar 3 7751',), {}),
    (('ne 5 14.32m',), {'ncrit': 4.7}),
    (('ne 5 24.31m',), {'ncrit': 4.4}),
    (('O  6 1032',), {'isForbidden':False}),
    (('Ne 6 7.652m',),{'ncrit':5.5}),#forbidden?
    (('Ne 8 770.4',),{'isForbidden':False}),
    (('Ne 8 780.3',),{'isForbidden':False}),
    (('TOTL 774.0',),{'isForbidden':False}),
    (('O  4 789.0',),{'isForbidden':False}),
    (('O  4 779.9',),{'isForbidden':False}),        
    (('C  3 1907',),{'ncrit':9.5}),
    (('C  3 1910',),{'ncrit':9.5}),
    (('TOTL 1909',),{'ncrit':9.5}),
    (('C  4 1548',),{'isForbidden':False}),    
    (('C  4 1551',),{'isForbidden':False}),
    (('TOTL 1549',),{'isForbidden':False}),
    (('He 2 1640',),{'isForbidden':False}),
    (('S  3 9532',),{'ncrit':5.8}),
    (('O  4 25.88m',),{'ncrit': 3.7}),
    (('Al 9 2.040m',),{'ncrit':8.3}),
    (('Si 6 1.963m',),{'ncrit':8.8}),
    (('Fe 2 1.257m',),{'ncrit':5.0}),
    (('Si 7 2.481m',),{}),#forbidden?
    (('S  3 18.67m',), {'ncrit': 4.2}),
    (('S  3 33.47m',), {'ncrit': 3.6}),
    (('Ne 2 12.81m',),{}), #forbidden?
    (('Ne 3 15.55m',), {'ncrit': 5.5}),
    (('Ne 3 36.01m',), {'ncrit': 4.7}),
    (('h  1 1.282m',), {'label': r'$Pa\beta$'}),
    (('h  1 1.875m',), {'label': r'$Pa\alpha$'}),
    (('O  1 63.17m',), {'ncrit': 4.7}),
    (('O  7 22.10',),{}),
    (('CaF1 7291',), {}),
    (('TOTL 2798',), {}),
] 
def loadHMspectra(fn = 'HM12_bg_spectrum.out'):
    ls = u.filelines(fn)[20:] 
    zs = map(float, string.split(ls[0])[1:])
    cols = u.unzip([[float(x) for x in string.split(l)] for l in ls[1:]])
    return [HMspectrum(z=zs[i],wls=cols[0], intensities=cols[i+1]) for i in rl(zs)]

class HMspectrum(SED):
    def copyConstructor(self,other):
	SED.copyConstructor(self,other=other)
	self.z = other.z    
    def __init__(self, z=0., wls=None, intensities=None,other=None):
	if other!=None: self.copyConstructor(other)	    
	else:
	    self.z=z
	    SED.__init__(self, (wls,intensities), waveunit='A', intensityunit='Jnu')
    def u_ion(self,maxRydberg=1000.):
	rydberg = 1/1.0968e5 #cm
	Rydbergnu = 3e10/rydberg	
	return 4*pi* self.integrate(self.Jnus(), (Rydbergnu,Rydbergnu*maxRydberg)) / 3e10
    def Prad2k(self,maxRydberg=1000.):
	kB = 1.3806488e-16 #erg K^-1	
	return self.u_ion(maxRydberg) / 3 / kB
    def __str__(self):
	return "HM spectrum z=%.2f"%self.z
    def __repr__(self):
	return self.__str__()
    def plot(self,yax='energy',normalize=False,ions=('HII','HeIII','CIV','OVI')):
	u.labelsize(True)
	if yax=='energy': 
	    yvals = self.nuJnus()*4*pi/3e10
	    ylabel(r'$\nu u_\nu~({\rm ergs~cm^{-3}})$')
	if yax=='intensity':
	    yvals = self.Jnus()
	    ylabel(r'$J_\nu {\rm ergs~s^{-1}~cm^{-2}~Hz^{-1}~sr^{-1}}$')
	plot(self.rydbergs(),yvals,label='z=%.2f'%self.z)
	loglog()
	[axvline(Cloudy.Ion(ion).ionizationEnergy(),c='k',ls='--') for ion in ions]
	xlabel(r'$h\nu~ ({\rm Ryd})$')
    def modify(self,min_nu,min_nu_modification,modification_powerlaw_index):
	s = self.nuInd(min_nu)-2
	self._intensities[s:] *= min_nu_modification * (self.nus()[s:]/min_nu)**modification_powerlaw_index

emissionLines = [EmissionLine(*x[0],**x[1]) for x in LineNames]
emissionLinesDic = dict([(l.key(),l) for l in emissionLines])
