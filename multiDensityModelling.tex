\documentclass[a4paper,10pt]{article}


\usepackage{graphicx,amssymb,amsmath,amsfonts}
\usepackage{color,subfigure}
\usepackage{natbib}
\usepackage[section]{placeins}
\usepackage{cases}
\usepackage[mathscr]{euscript}
\usepackage{mathrsfs}

% \input{commands.tex}

\newcommand{\Ox}{{\rm O}}
\newcommand{\Si}{{\rm Si}}
\newcommand{\N}{{\rm N}}
\newcommand{\C}{{\rm C}}
\newcommand{\Hy}{{\rm H}}
\newcommand{\NHscale}{N_{0}}
\newcommand{\AMD}{\deriv \NH / \deriv (\log \rho)}
\newcommand{\AMDfrac}{\frac{\deriv \NH}{\deriv \log \rho}}
\newcommand{\nhi}{N_{\rm \hi}}
\newcommand{\novi}{N_{\rm \ovip}}
\newcommand{\nciii}{N_{\rm \ciiip}}
\newcommand{\nciv}{N_{\rm \civp}}
\newcommand{\nsiiv}{N_{\rm \Siivp}}
\newcommand{\nsiiii}{N_{\rm \Siiiip}}
\newcommand{\nfeiii}{N_{\rm \feiiip}}
\newcommand{\nnii}{N_{\rm \niip}}
\newcommand{\jn}{j_{\rm \nH}}
\newcommand{\rn}{r_{\rm \nH}}
\newcommand{\Mgas}{M_{\rm gas}}
\newcommand{\Mstar}{M_*}
\newcommand{\rvir}{R_{\rm vir}}
\newcommand{\rmin}{r_{\rm min}}
\newcommand{\cfn}{{\rm CF}_{\nH}}
\newcommand{\cfz}{{\rm CF}_{0}}
\newcommand{\dNz}{N_0}
\newcommand{\nz}{n_{{\rm H},0}}
\newcommand{\rz}{r_{c,0}}
\newcommand{\nmax}{n_{\rm H,max}}
\newcommand{\nmin}{n_{\rm H,min}}
\newcommand{\phii}{\phi}
\newcommand{\phiHM}{\phi_{{\rm HM12}}}
\newcommand{\phiK}{\phi_{{\rm K14}}}
\newcommand{\Rimp}{R_{\perp}}
\newcommand{\Nexp}{N_{\rm pred}}
\newcommand{\Nobs}{N_{\rm obs}}
\newcommand{\CF}{f_{\rm C}}
\newcommand{\Mscr}{\mathscr{M}}
\newcommand{\Ncl}{N_c}
% \newcommand{\Mcgm}{M_{\rm CGM}}
% \newcommand{\NHbar}{\bar{N}_{\rm H}}

\newcommand{\Mcool}{M_{{\rm cool}}}
\newcommand{\Mcooli}{M_{{\rm cool},i}}
\newcommand{\rc}{r_{\rm c}}
\newcommand{\nc}{n_{\rm c}}
\newcommand{\sigc}{\sigma_{\rm c}}
\newcommand{\rci}{r_{{\rm c},i}}
\newcommand{\rcim}{r_{{\rm c},i-1}}
\newcommand{\nci}{n_{{\rm c},i}}
\newcommand{\Nci}{N_{{\rm c},i}}
\newcommand{\Vci}{V_{i}}
\newcommand{\Vcim}{V_{i-1}}
\newcommand{\fV}{f_{{\rm V}}}
% \newcommand{\fV}{\epsilon}
\newcommand{\fVz}{f_{{\rm V},0}}
\newcommand{\fVi}{f_{{\rm V},i}}
\newcommand{\fViz}{f_{{\rm V},i,\rvir}}
\newcommand{\fVim}{f_{{\rm V},i-1}}
\newcommand{\CFi}{f_{{\rm C},i}}
\newcommand{\CFim}{f_{{\rm C},i-1}}
\newcommand{\sigci}{\sigma_{i}}
\newcommand{\sigcim}{\sigma_{i-1}}
\newcommand{\nHi}{n_{{\rm H},i}}
\newcommand{\nHim}{n_{{\rm H},i-1}}
\newcommand{\rhoi}{\rho_{i}}
\newcommand{\rhoim}{\rho_{i-1}}
\newcommand{\rhomin}{\rho_{\rm min}}
\newcommand{\rhomean}{\rho_{\rm mean}}
\newcommand{\rhomax}{\rho_{\rm max}}
\newcommand{\Rmin}{R_{\rm min}}
\newcommand{\AMDav}{\langle\deriv\NH/\deriv\log\rho\rangle}
\newcommand{\NHavRimp}{\langle\NH(\Rimp)\rangle}
\newcommand{\AMDavfrac}{\left\langle\frac{\deriv\NH}{\deriv\log\rho}\right\rangle}
\newcommand{\nHbar}{{\bar \nH}}
\newcommand{\el}{{\rm X}}
% \renewcommand{\ion}{\el^{i+}}
% \renewcommand{\fion}{f_{\ion}}
\newcommand{\Nion}{N_{\ion}}
\newcommand{\rhoz}{\rho_0}
\newcommand{\rhocool}{\rho_{\rm cool}}
\newcommand{\rhocoolmin}{\rho_{\rm cool; min}}
\newcommand{\rhocoolmax}{\rho_{\rm cool; max}}
\newcommand{\li}{\lambda_i}
\newcommand{\plowion}{P_{\rm low-ion}}
\newcommand{\povi}{P_{\rm \ovip}}
\newcommand{\Thot}{T_{\rm hot}}
\newcommand{\Tcool}{T_{\rm cool}}
\newcommand{\fg}{f_{\rm gas}}

\newcommand{\nH}{n_{\rm H}}
\newcommand{\NH}{N_{\rm H}}
\renewcommand{\d}{{\rm d}}
\renewcommand{\Nion}{N_{\rm ion}}

\title{Multi-density Photoionization Modeling}
\author{Jonathan Stern}

\begin{document}
\maketitle

\section{Parametrization of multi-density gas}
Observations gives us a set of ion columns $\{\Nion^{\rm (obs)}\}$. In the classic single-density models, these ion columns are modeled using the gas hydrogen column $\NH$, the ionization parameter $U$ (or equivalently the gas density $\nH$ for an assumed ionizing background), and the metallicity $Z$. 
Generalizing to absorbers which span a range of densities, we can replace the parameters $\NH$ and $\nH$ with a more general property:
\begin{equation}\label{e:AMD}
 \frac{\d\NH}{\d \log \nH}(\nH) = 
\begin{bmatrix}
N_0 \left(\frac{\nH}{n_0}\right)^\beta & n_0 < \nH < n_{\rm max} \\
0 & otherwise
\end{bmatrix}
\end{equation}
namely the column density per decade in volume density. This property is known as the Absorption Measure distribution (AMD, see Behar et al.\ 2009, Stern et al.\ 2016). In eqn.~(\ref{e:AMD}), I approximate the AMD as a power-law with four free parameters: the normalization $N_0$, the index $\beta$, and the minimum and maximum densities spanned by the power-law $n_0$ and $n_{\rm max}$. 
Thus, by replacing the two free parameters $\NH$ and $\nH$ of the single density models with the four free parameters of the AMD, one can derive a more physically-motivated constraint on the properties of CGM clouds. 



\section{Relation to physical parameters of CGM clouds}

In this section I derive the relation between the AMD and more intuitive parameters of CGM clouds:  gas hydrogen density $\nH$, cloud distance from the galaxy $R$, and cloud volume filling factors $\fV$. 

Assume the density of cool clouds in the CGM have a power-law dependence on $R$:
\begin{equation}\label{e:nH}
 \nH = n_0 \left(\frac{R}{\rvir}\right)^\gamma
\end{equation}
where $\rvir$ is the virial radius of the dark matter halo. Also, assume a power-law dependence of the filling factor on $R$
\begin{equation}
 \fV = f_0 \left(\frac{R}{\rvir}\right)^\delta
\end{equation}
For a line of sight with impact parameter $\Rimp$ and line element $\d s \equiv \d R / \sqrt{1-(\Rimp/R)^2}$, on an `average' sight line we get
\begin{equation}\label{e:NH}
 \NH = \int_{-\infty}^{+\infty} \nH\fV\d s = n_0 f_0\int_{-\infty}^{+\infty}\left(\frac{R}{\rvir}\right)^{\gamma + \delta}\d s
\end{equation}
By putting $\fV$ within the intergal, we are computing the average AMD of an ensemble of sightlines. A specific sightline may intersect a cloud at a given $R$ or it may not. This dispersion around the mean is addressed below. 

With a little algebra detailed in the appendix, one can use this equation to show that for $\Rimp=0$ the AMD is a power law of the form 
\begin{equation}
 \frac{\d \NH}{\d \log \nH}(\Rimp=0) = N_0 \left(\frac{\nH}{n_0}\right)^\beta
\end{equation}
with 
\begin{equation}
 N_0 = \frac{2 \ln (10)\rvir n_0 f_0}{\gamma}, ~~ \beta = \frac{1+\delta+\gamma}{\gamma}
\end{equation}
For general $\Rimp$ we get
\begin{equation}
  \frac{\d \NH}{\d \log \nH} =  
\begin{bmatrix} 
N_0 \left(\frac{\nH}{n_0}\right)^{\beta} \cdot \frac{1}{\sqrt{1-\left(\frac{\nH(\Rimp)}{\nH}\right)^{2/\gamma}}} & \nH < \nH(\Rimp) \\
0 & \nH > \nH(\Rimp) 
\end{bmatrix} 
\approx
\begin{bmatrix} 
N_0 \left(\frac{\nH}{n_0}\right)^{\beta} & \nH < \nH(\Rimp) \\
0 & \nH > \nH(\Rimp) 
\end{bmatrix}  
\end{equation}


\section{Individual sightlines vs.\ average sightlines}

\textbf{TBD}

\section{Appendix: algebra}

Starting with the simplest case where $\Rimp=0$, we can replace the $\int_{-\infty}^{+\infty} ... \d s$ in eqn.~(\ref{e:NH}) with $2\int_{0}^{\infty} ... \d R$:
\begin{equation}\label{e:NH Rimp0}
 \NH(\Rimp=0) = 2 n_0 f_0\int_{0}^{\infty}\left(\frac{R}{\rvir}\right)^{\gamma + \delta}\d R 
\end{equation}
Now, since eqn.~(\ref{e:nH}) and its derivative imply
\begin{equation}
 \frac{R}{\rvir} = \left(\frac{\nH}{n_0}\right)^{1/\gamma}, ~~ \d R = \frac{\rvir}{n_0\gamma}\left(\frac{\nH}{n_0}\right)^{\frac{1}{\gamma}-1}\d \nH
\end{equation}
then eqn.~(\ref{e:NH Rimp0}) is equal to
\begin{equation}\label{e:NH vs nH}
 \NH(\Rimp=0) = \frac{2 \rvir f_0}{\gamma}\int_{-\infty}^{+\infty}\left(\frac{\nH}{n_0}\right)^{\frac{1+\delta}{\gamma}}\d \nH
\end{equation}
Dividing by $\d\nH$ we get
\begin{equation}\label{e:dNH/dnH}
 \frac{\d \NH}{\d \nH}(\Rimp=0) = \frac{2 \rvir f_0}{\gamma} \left(\frac{\nH}{n_0}\right)^{\frac{1+\delta}{\gamma}} 
\end{equation}
so the AMD equals
\begin{equation}
 \frac{\d \NH}{\d \log \nH}(\Rimp=0) = \ln (10) \nH \frac{\d \NH}{\d \nH}(\Rimp=0) =  \frac{2 \ln (10)\rvir n_0 f_0}{\gamma} \left(\frac{\nH}{n_0}\right)^{\frac{1+\delta+\gamma}{\gamma}}
\end{equation}
or equivalently
\begin{equation}
 \frac{\d \NH}{\d \log \nH}(\Rimp=0) = N_0 \left(\frac{\nH}{n_0}\right)^\beta
\end{equation}
with 
\begin{equation}\label{e:N0 beta}
 N_0 = \frac{2 \ln (10)\rvir n_0 f_0}{\gamma}, ~~ \beta = \frac{1+\delta+\gamma}{\gamma}
\end{equation}

For general $\Rimp$, we can use the equality 
\begin{equation}
\d s= \frac{\d R}{\sqrt{1-(\Rimp/R)^2}} = \frac{\d R}{\sqrt{1-(\nH(\Rimp)/\nH(R))^{2/\gamma}}}
\end{equation}
in eqn.~(\ref{e:NH}), 
so eqn.~(\ref{e:NH vs nH}) has the more general form 
\begin{equation}
 \NH = \frac{2 \rvir f_0}{\gamma}\int_{-\infty}^{+\infty}\left(\frac{\nH}{n_0}\right)^{\frac{1+\delta}{\gamma}}\frac{\d \nH}{\sqrt{1-\left(\frac{\nH(\Rimp)}{\nH}\right)^{2/\gamma}}}
\end{equation}
Using a similar derivation as in eqns.~(\ref{e:dNH/dnH})--(\ref{e:N0 beta}) we get
\begin{equation}
  \frac{\d \NH}{\d \log \nH} =  
\begin{bmatrix} 
N_0 \left(\frac{\nH}{n_0}\right)^{\beta} \cdot \frac{1}{\sqrt{1-\left(\frac{\nH(\Rimp)}{\nH}\right)^{2/\gamma}}} & \nH < \nH(\Rimp) \\
0 & \nH > \nH(\Rimp) 
\end{bmatrix} 
\end{equation}
which can be approximated as 
\begin{equation}
\frac{\d \NH}{\d \log \nH} \approx
 \begin{bmatrix} 
N_0 \left(\frac{\nH}{n_0}\right)^{\beta} & \nH < \nH(\Rimp) \\
0 & \nH > \nH(\Rimp) 
\end{bmatrix}  
\end{equation}
The accuracy of this approximation is demonstrated in Figure~1 (\textbf{TBD}). 








\end{document}


